var ts = {
  dev: {
    src: ['*.ts','src/ts/**/*.ts'],
    options: {
      target: 'es6',
      module: 'commonjs',
      sourceMap: false
    }
  }
}

module.exports = function (grunt) {
  grunt.config.set('ts', ts);
}
