var watch = {
  ts: {
    files: ['*.ts','src/ts/**/*.ts'],
    tasks: ['ts:dev'],
    options: {
      livereload: true,
      debug: false,
      debounceDelay: 100
    }
  },
  sass: {
    files: ['src/sass/**/*.scss'],
    tasks: ['styles'],
    options: {
      livereload: true,
      debug: false,
      debounceDelay: 100
    }
  }
}
module.exports = function (grunt) {
  grunt.config.set('watch', watch);
}
