var copy = {
  external: {
    files: [
      // jquery
      { expand: true, cwd: 'external/jquery/dist', src: '**', dest: 'src/lib/jquery' },
      // jquery-ui
      { expand: true, cwd: 'external/jquery-ui/', src: ['jquery-ui.js'], dest: 'src/lib/jquery-ui' },
      { expand: true, cwd: 'external/jquery-ui/themes/base', src: ['jquery-ui.css', 'images/*'], dest: 'src/lib/jquery-ui' },
     
      // lodash
      { expand: true, cwd: 'external/lodash', src: ['lodash.js'], dest: 'src/lib/lodash/' },
      // momentjs
      { expand: true, cwd: 'external/momentjs/min', src: ['moment-with-locales.js'], dest: 'src/lib/momentjs/' },
     
      // PruneCluster
      { expand: true, cwd: 'external/PruneCluster/dist', src: ['LeafletStyleSheet.css', 'PruneCluster.js', 'PruneCluster.js.map'], dest: 'src/lib/PruneCluster' },
     
      // eventemitter2
      { expand: true, cwd: 'external/eventemitter2/lib', src: ['eventemitter2.js'], dest: 'src/lib/eventemitter2/' },
      // socket.io-client
      { expand: true, cwd: 'external/socket.io-client', src: ['socket.io.js'], dest: 'src/lib/socket.io-client/' },
      // modernizr
      { expand: true, cwd: 'external/modernizr', src: ['modernizr.js'], dest: 'src/lib/modernizr' },
      // object-observe
      { expand: true, cwd: 'external/object.observe/dist', src: ['object-observe.js'], dest: 'src/lib/object.observe'},
      // modernizr-load
      { expand: true, cwd: 'external/modernizr-load', src: ['modernizr.js'], dest: 'src/lib/modernizr-load' },
      // promise
      { expand: true, cwd: 'external/promise-js', src: ['promise.js'], dest: 'src/lib/promise-js' },
      // Leaflet.encoded
      { expand: true, cwd: 'external/Leaflet.encoded', src: ['Polyline.encoded.js'], dest: 'src/lib/Leaflet.encoded' }
    ]
  },
  release: {
    files: [
      { expand: true, cwd: 'src', src: ['**', '!css/**', '!lib/**', '!leaflet/**', '!sass/**', '!scripts/**', '!ts/**', '!patches/**'], dest: 'dist/release' },
      { expand: true, cwd: 'src/lib/jquery-ui', src: ['images/*'], dest: 'dist/release/css' }, // niestety jquery-ui.css wciaga obrazki relatywnie do siebie
      { expand: true, cwd: 'src/leaflet', src: ['images/*'], dest: 'dist/release/css' },
      { expand: true, cwd: 'src/lib/promise-js', src: ['promise.js'], dest: 'dist/release/lib/promise-js' },     
      // { expand: true, cwd: 'src/lib', src: ['**/*', '!**/*.js', '!**/*.css', '!jquery-ui/images/*'], dest: 'dist/release', filter: 'isFile',
      //   rename: function(dest, src) {
      //     console.log(src, '=>', dest);
      //   var path = require('path');
      //   var pathSep = "/"; //path.sep - nie wiem dlaczego na windowsie pliki sa rozdzielone "/", zapisuje na sztywno
      //   // Remove the first 1 folders from src and place into dest
      //   var wobase = src.split(pathSep).slice(1).join(path.sep);
      //   var joined = path.join(dest, wobase);
      //     return joined;
      // }},
    ]
  },
  deploy: {
    files: [
       { expand: true, cwd: 'dist/release', src: ['**'], dest: '../smartsab_mobile/www' }
    ]
  }
};

module.exports = function (grunt) {
  grunt.config.set('copy', copy);
}
